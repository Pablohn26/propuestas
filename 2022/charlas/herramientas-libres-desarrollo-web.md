---
layout: 2022/post
section: proposals
category: talks
author: Pablo Arias
title: Herramientas libres para el desarrollo web
---

# Herramientas libres para el desarrollo web

> Llevo años empleando software libre para la realización de sitios web. Quiero compartir las diversas herramientas libres que empleo para este fin. Algunas de ellas son de propósito general y le sirven a todo el mundo. Otras son más específicas para el mundo de la web.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> Voy a mostrar que es posible hacer sitios web únicamente con software libre. También quiero que se vea que las herramientas disponibles no tienen nada que envidiar a las propietarias.<br><br>
No solo a nivel CMS, que uso Joomla y hay muchos más en este ámbito, sino a todos los niveles: desde el sistema operativo hasta la herramienta para guardar contraseñas, pasando por el navegador, gestión de versiones, edición de imagen, etc, etc.

-   Público objetivo:

> Los desarrolladores web se sentirán especialmente identificados con el título pero es una charla transversal que puede ayudar a cualquiera para el trabajo de oficina en su día a día.

## Ponente:

-   Nombre: Pablo Arias
-   Bio:

> Desarrollador de sitios web y especializado en la mejora continua de estos. Proporciono asesoría estratégica y dirección de proyectos web. Primer certificado en Joomla a nivel nacional.

### Info personal:

-   Web personal: <https://www.pabloarias.eu/>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://www.pabloarias.eu/webs>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
