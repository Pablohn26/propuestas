---
layout: 2022/post
section: proposals
category: talks
author: Jose Manrique López de la Fuente
title: Sostenibilidad y Oficinas Open Source
---

# Sostenibilidad y Oficinas Open Source

> La sostenibilidad se ha convertido en uno de los principales valores de muchas empresas modernas como muestra de su compromiso con el ecosistema medioambiental en el que están ¿Y si extendemos la sostenibilidad hacia otros ecosistemas como el tecnológico?<br><br>
¿Cómo pueden las Oficinas Open Source ayudar a fomentar este concepto en las empresas o cualquier organización moderna?

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> El objetivo de esta charla es presentar y discutir términos como oficinas open source, sostenibilidad, o ecosistema tecnológico desde el punto de vista de las empresas u organizaciones.<br><br>
Los recientes debates sobre los desequilibrios entre proyectos open source, entidades usuarias, administraciones, problemas de mantenibilidad de los proyectos, etc. son un reflejo del grado de importancia que ha llegado a tener el software libre en la sociedad moderna.<br><br>
Pero esa importancia conlleva una responsabilidad por parte de la sociedad en cómo se consume, cómo se contribuye, y cómo se gestionan proyectos open source. Las oficinas open source pueden ser uno de los mecanismos que ayuden a dar el siguiente paso de madurez desde organizaciones y entidades.<br><br>
Durante esta charla se presentarán algunas ideas y ejemplos para poder discutirlos, así como comunidades donde participar para dinamizar este tipo de iniciativas.

-   Público objetivo:

> Personas gestionando una oficina open source, gestionando áreas de innovación tecnológica en empresas, y general cualquier persona interesada en discutir sobre mecanismos para mejorar la sostenibilidad del ecosistema tecnológico en cualquier ámbito o alcance.

## Ponente:

-   Nombre: Jose Manrique López de la Fuente

-   Bio:

> Manrique es responsable de la OSO (Open Source Office) en Inditex/ZaraTech Digital, y un apasionado del software libre y las comunidades de desarrollo. Es ingeniero industrial, con experiencia en investigación y desarrollo en el Centro Tecnológico de Informática y Comunicaciones del Principado de Asturias (CTIC), grupos de trabajo del W3C, Ándago Engineering y Continua Health Alliance. Ex-director ejecutivo de la Asociación Española de Empresas de Código Abierto (ASOLIF), ex-consultor experto del Centro Nacional de Referencia de Código Abierto (CENATIC), y ex-CEO y socio de Bitergia.<br><br>
Involucrado en varias comunidades relacionadas con el software libre, libre y de código abierto, ha sido reconocido como AWS Data Hero y GitLab Community Hero. <br><br>
Puedes encontrarle en la red como @jsmanrique, y cuando no está en línea, le encanta pasar tiempo con la familia haciendo surf.

### Info personal:

-   Web personal: <https://jsmanrique.es>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@jsmanrique>
-   Twitter: <https://twitter.com/jsmanrique>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://gitlab.com/jsmanrique> / <https://github.com/jsmanrique>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
