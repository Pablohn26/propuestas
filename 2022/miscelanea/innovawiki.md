---
layout: 2022/post
section: proposals
category: misc
author: Florencia Claes
title: InnovaWiki
---

# InnovaWiki

> Se trata de un póster que explica los recursos creados de un grupo de innovación docente que lleva adelante diferntes acciones educativas valiéndose del entorno Wikimedia.

## Detalles de la propuesta:

-   Tipo de propuesta: Póster

-   Descripción:

> Desde el grupo de innovación docente de la Universidad Rey Juan Carlos, InnovaWiki, trabajamos en el diseño de metodologías educativas utilizando las plataformas de los proyectos Wikimedia.<br><br>
Somos un grupo de 9 profesores, la mayoría del área de la Comunicación, que promovemos el aprendizaje colaborativo, el trabajo con materiales libres, y la creación y verificación de contenidos para enriquecer entornos abiertos y a disposición de la ciudadanía.<br><br>
Diseñamos itinerarios formativos a partir del uso, lectura crítica, edición y mejora de los contenidos de los proyectos Wikimedia. Entendemos que estos diseños deben ser compartidos más allá de las aulas de nuestra universidad y estar a disposición de la ciudadanía en general y del cuerpo docente en particular. Para ello, estamos desarrollando una página web que funciona de agregador de cada uno de esos itinerarios, que son fácilmente adaptables a otros contextos y situaciones.<br><br>
En el póster se presentará la web de InnovaWiki, haciendo especial hincapié en los itinerarios formativos más utilizados y que consideramos que son más fácilmente replicables.

-   Web del proyecto: <http://innovawiki.es/>

-   Público objetivo:

> Docentes, personas relacionadas con la enseñanza, personas que desarrollen actividades en torno a Wikipedia y sus proyectos hermanos.

## Ponente:

-   Nombre: Florencia Claes

-   Bio:

> Florencia Claes, doctora en Comunicación audiovisual y publicidad (UCM 2015), desarrolla su actividad docente e investigadora en la Universidad Rey Juan Carlos desde el año 2017 en el Departamento de Comunicación Audiovisual de la Facultad de Comunicación. Desde 2021 es responsable de Cultura Libre en la Oficina de Cultura y conocimiento libres de la URJC.<br><br>
Colabora activamente con los proyectos de la Fundación Wikimedia, y desde 2017 forma parte de la Junta directiva de Wikimedia España, de la que es presidenta desde 2021. Dirige el Laboratorio Wikimedia de verificación de datos en Madialab Matadero y coordina diferentes proyectos de innovación educativa.<br><br>
También ha trabajado 10 años en la productora Globomedia, donde participó en el desarrollo y comunicación de diferentes proyectos cinematográficos como Pájaros de papel, A night in Old Mexico o Carlitos y el campo de los sueños entre otros.<br><br>

### Info personal:

-   Web personal: <https://florenciaclaes.com/>
-   Twitter: <https://twitter.com/@FlorenClaes>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
