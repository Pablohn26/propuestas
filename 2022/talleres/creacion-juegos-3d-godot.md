---
layout: 2022/post
section: proposals
category: workshops
author: Alvaro del Castillo San Félix
title: Creación de Juegos 3D en Godot
---

# Creación de Juegos 3D en Godot

> Godot es un motor de videojuegos Open Source utilizado a nivel profesional. Permite el desarrollo de juegos en 2D y 3D que puede ser ejecutados en diversas plataformas (ordenadores, móviles, consolas) desde un editor visual y permitiendo el desarrollo en diferentes lenguajes de programación como GDScript, C++ o C#. En este taller nos centraremos en la creación de un juego en 3D desde cero.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial

-   Descripción:

> Durante el taller el objetivo es que los participantes sean capaces de:
* Manejar la interfaz en 3D de Godot
* Utilizar modelos en 3D
* Entender como utilizar los motores de física para implementar gran parte de la lógica de los juegos
* Poder seguir el tutorial de 3D de Godot y realizar modificaciones sobre el juego https://github.com/godotengine/godot-3d-dodge-the-creeps<br><br>
La duración estimada la llevaría a las 2h ya que sea como sea, nos va a faltar tiempo :)<br><br>
En el pasado ya realicé un taller de Godot para la creación de juegos en 2D en esLibre: <https://www.youtube.com/watch?v=PdJOBa5UGWI&list=PLRDwepmU5mR-3gDFe1-ueKmrCq2ade0Rn>

-   Web del proyecto: <https://godotengine.org/>

-   Público objetivo:

> Personas interesadas en el desarrollo de videojuegos y, en general, del desarrollo de contenidos interactivos en 3D.

## Ponente:

-   Nombre: Alvaro del Castillo San Félix

-   Bio:

> Tech has been my passion during my lifetime, and applying them to new challenges a core motivation for me. Sharing this passion thanks to Open Source has been killer. My favorite time? Execution: design, coding and deploying with talented teams with the right technologies to achieve ambitious goals.<br><br>
A few years ago I started to develop a framework for creating worlds in Minecraft (<https://github.com/Voxelers/mcthings>) and I started to get in love with Voxels and 3D worlds and, in general, with the Computer Graphics. Currently, this is a hobby where I want to invest more and more time.

### Info personal:

-   Web personal: <https://www.linkedin.com/in/acslinkedin/>
-   Twitter: <https://twitter.com/acstw>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/acs/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
