---
layout: 2020/post
section: proposals
category: talks
title: Introducción a ProxySQL
---

En un servicio no hay nada tan problemático y crítico como la capa de persistencia, una parte tan vital que además siempre admite mejora. En esta charla hablaremos un poco de que es ProxySQL, que herramientas ofrece, y como podemos usarlas para mejorar nuestra infraestructura.

## Formato de la propuesta

-   [x]  Charla (25 minutos)
-   [ ]  Charla relámpago (10 minutos)

## Descripción

ProxySQL es un proxy open source de alta disponibilidad, alto rendimiento y consciente de protocolo para MySQL. La charla consistirá en 5-8 minutos de introducción sobre ProxySQL como proyecto, sus características, y la topología básica de su uso. Posteriormente cubriremos en más detalle algunas de sus características principales, viendo ejemplos de como usarlas:

-   Cambios de configuración con Zero-downtime

      ProxySQL posee tres capas de configuración diferentes: memory, disk y runtime. Las cuales pueden configurarse dinámicamente, con zero-downtime.

    > Configuraremos en directo ProxySQL, para ver como se realizan los diferentes cambios en su configuración.

-   Proxy de Nivel de Aplicación

      ProxySQL no es solo un balanceador de carga, entiende el protocolo MySQL y provee manejo de conexiones end-to-end, estadísticas tiempo real, e inspección de tráfico.

    > Veremos como configurar fácilmente ProxySQL para hacer de load-balancer contra un cluster de MySQL y hacer uso de sus estadísticas.

-   Database Firewall

      ProxySQL es capaz de actuar como un firewall SQL, añadiendo granularidad de configuración en como definir el acceso y protecciones de nuestras bases de datos.

    > Configuraremos unas sencillas reglas y comprobaremos como ProxySQL defiende la DB de accesos o consultas no permitidas.

-   Advanced Query Rules

      ProxySQL permite definir complejas reglas de enrutamiento para las consultas, y cachear el resultado de las mismas.

    > Crearemos unas sencillas reglas de enrutamiento y verificaremos como ProxySQL es capaz de redirigir nuestras consultas en un cluster de MySQL.

-   Failover detection

      ProxySQL es capaz de automáticamente detectar cambios en la topología del cluster que está monitorizando, y de redirigir tráfico en función de estos.

    > Usaremos como ejemplo un Galera cluster, para ver como ProxySQL lo monitoriza y reacciona a cambios en la topología del mismo.

-   Mirroring / Garantizado de consistencia de lectura / Etc... !

    > Describiremos brevemente usando el tiempo restante más características realmente interesantes que ProxySQL ofrece.

## Público objetivo

Aquellos con un rol cercano DevOps/DBAs o developers interesados en infraestructuras de backend en general. La charla intentará tener un carácter introductorio y si bien, conocimientos básicos de administración de sistemas y bases de datos son necesarios, cualquiera con un mínimo de estos conocimientos debería poder de seguirla conceptualmente.

## Ponente(s)

**Javier Jaramago Fernández**, Software Engineer @ [ProxySQL](https://proxysql.com/).

### Contacto(s)

-   **Javier Jaramago Fernández**: javier at proxysql dot com | [@JavierJF](https://github.com/JavierJF) (GitHub) |

## Comentarios

Los 25 minutos parecen algo escasos para todo el contenido que había pensado meter en la charla. ¿Quizás hay alguna forma de dividirla o movernos a una sala aparte con los interesados que quisieran saber más después de los 25 min?

Muchas gracias.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x]  Al menos una persona entre los que la proponen estará presente el día programado para la charla.
