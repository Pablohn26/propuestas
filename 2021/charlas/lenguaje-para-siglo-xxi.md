---
layout: 2021/post
section: proposals
category: talks
author: JJ Merelo
title: Un lenguaje para el siglo XXI
---

Los lenguajes de programación van naciendo, creciendo, cambiando, y en todo momento reflejan el _zeitgeist_ de la época en la que se han hecho. Cada lenguaje tiene su visión, y en esta charla hablaremos de [Raku](https://raku.org), un lenguaje diseñado para los próximos 100 años, y que tiene las características de un lenguaje multipradigma, expresivo y que haga fácil la programación creativa.

## Formato de la propuesta

Indicar uno de estos:

-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Raku nació en el año 2000 de la mano de Larry Wall y la comunidad de Perl; inicialmente como una próxima versión del mismo, eventualmente como un lenguaje totalmente independiente y propio. Nacido a partir de una serie de propuestas de la comunidad, se incorporaron conceptos de múltiples fuentes haciéndolo un verdadero lenguaje multiparadigma y abierto a la evolución. En este trabajo hablaremos de estas características de lenguajes modernos, y cómo se implementan en Raku.

-   Web del proyecto: <https://raku.org>

## Público objetivo

Público con interés en programación.

## Ponente(s)

JJ Merelo, programador desde 1983.

### Contacto(s)

-   Nombre: JJ Merelo
-   Email: <jjmerelo@gmail.com>
-   Web personal: <https://jj.github.io/>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/jjmerelo>
-   Gitlab: <https://gitlab.com/JJ5>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/JJ>

## Comentarios
