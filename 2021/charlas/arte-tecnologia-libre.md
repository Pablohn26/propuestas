---
layout: 2021/post
section: proposals
category: talks
author: Paula de la Hoz
title: Arte+Tecnología (libre)
---

Selección, análisis y expresión de colores, interacción con el espectador, accesibilidad... Hay muchas barreras que pueden atravesarse en el arte usando electrónica y tecnología libre, y en esta charla se proponen algunas formas de mezclar las técnicas más tradicionales del arte con software y hardware libre.  

## Formato de la propuesta

Indicar uno de estos:

-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Para comenzar se explicaran algunas de las cuestiones que mueven el arte contemporaneo y algunas influencias del pasado siglo, para después unirlas con nuevos conceptos. Se explicarán algunas técnicas de expresión con color y  como experimentar una mezcla de esas técnicas con luz, o cómo generar un análisis de la paleta de colores (y como de balanceada está) con Python, de modo que podamos inspirarnos en la paleta de nuestras artistas favoritas. Se explicará como crear montajes interactivos artísticos con hardware libre que acerquen e impliquen al espectador en la obra y como crear herramientas personalizadas que nos asistan en el proceso de creación de arte. La charla pretende romper algunas barreras entre el arte y la tecnología.

-   Web del proyecto: <https://inversealien.space/arte>

## Público objetivo

Artistas, gente interesada en electrónica libre o personas curiosas en general. Todos los conceptos técnicos serán explicados en varios niveles para que se pueda disfrutar tanto a nivel divulgativo como a nivel técnico.

## Ponente(s)

Threat Hunter Analyst en Telefónica, anteriormente en el equipo de Offensive Security. Profesora de hardware hacking y pentesting en el FP de ciberseguridad de Salesianos Atocha y en el máster de Red Team y Blue Team de la UCAM. Impartí un seminario de electrónica en la facultad de BBAA de la UGR. Presidenta y cofundadora de Interferencias, una asociación sin ánimo de lucro por la defensa de la privacidad y los derechos digitales.

### Contacto(s)

-   Nombre: Paula de la Hoz
-   Email:
-   Web personal: <https://inversealien.space>
-   Mastodon (u otras redes sociales libres): <https://cybre.space/@terceranexus6/>
-   Twitter: <https://twitter.com/Terceranexus6>
-   Gitlab: <https://gitlab.com/terceranexus6>
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios
