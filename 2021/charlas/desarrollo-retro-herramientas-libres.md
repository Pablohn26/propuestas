---
layout: 2021/post
section: proposals
category: talks
author: Victor Suarez
title: Desarrollo de juegos retro con herramientas libres
---

Muchos recordaran los tiempos de las máquinas de 8 y 16 bits. Muchos hemos crecido con ellas y ahora nos preguntamos... ¿será posible de hacer algo para aquellas consolas u ordenadores que tanto nos dieron? Y aun mejor, ¿con herramientas libres? Esto y mucho más lo explicaremos en esta charla donde se mostrarán algunos proyectos libres de juegos y herramientas para desarrollarlos también libres.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

En esta charla se mostrarán cómo podemos hoy en día con las herramientas libres que disponemos, poder desarrollar para plataformas de 8 y 16 bits. Desde Spectrum, MSX o Atari ST, sin olvidar de las consolas como NES, Game Boy o la mítica Mega Drive. Hoy en día podemos encontrar juegos, herramientas y frameworks para poder desarrollar para estas plataformas.

Si eres un nostálgico de lo retro, esta es tu charla; donde podrás recordar viejos tiempos y ver cómo hoy en día aunque han pasado más de 30 años de algunas de estas plataformas, siguen estando vigentes y se sigue desarrollando para ellas.

## Público objetivo

Cualquier persona interesada en el mundo del desarrollo retro y el homebrew para dispositivos retro de 8 y 16 bits.

## Ponente(s)

Ingeniero en informática que siempre esta tratando de aprender cosas nuevas. Además de que le encanta el desarrollo retro y esta siempre tratando de organizar y dar charlas para que los demás aprendan.

### Contacto(s)

-   Nombre: Víctor Suárez García
-   Email: <zerasul@gmail.com>
-   Web personal: <https://zerasul.me>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/zerasul>
-   Gitlab:
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/zerasul>

## Comentarios
