---
layout: 2023/post
section: proposals
category: devrooms
author: Equipo de Competencia Digital Docente de CATEDU
title: Software libre y Competencia Digital Docente
---

# Software libre y Competencia Digital Docente

## Detalles de la propuesta:

>Recientemente se ha publicado el Marco de Referencia de Competencia Digital Docente (MRCDD) en España, y el profesorado está en pleno proceso para acreditar su competencia digital docente. Muchas grandes tecnológicas están aprovechando para incrementar su presión sobre el sector educativo vinculando la utilización de sus herramientas a la acreditación de dicha competencia digital. Esta sala mostraría las posibilidades que ofrece el software libre en cuanto a poner a disposición aplicaciones educativas punteras y respetuosas con la privacidad del alumnado y el profesorado, tanto a las administraciones educativas como al profesorado en general.

-   Formato:

>Duración: viernes por la tarde y sábado por la mañana.<br><br>
Se trabajarían las 6 áreas descritas en el MRCDD. Para cada área se alternaría según su naturaleza la realización de una charla o taller. <br><br>
Por último cerraría una mesa redonda con discusión sobre la necesidad de la inclusión del software libre en las escuelas, como espacios libres de negocio.

-   Público objetivo:

>Administraciones educativas de diferentes comunidades autónomas que estén impulsando la formación y acreditación en competencia digital docente, y se planteen impulsar herramientas digitales a la disposición de su profesorado para ello.<br><br>
Profesorado en general que quiera mejorar su competencia digital docente utilizando herramientas social y éticamente comprometidas con la comunidad, y respetuosas con sus datos y con los de su alumnado.

## Comunidad que propone la sala:

### Equipo de Competencia Digital Docente de CATEDU

> CATEDU es el Centro Aragonés de Tecnologías para la Educación. Se integran en él diversos servicios y plataformas puestas a disposición de la Comunidad Educativa aragonesa todas ellas basadas en software libre como Vitalinux, Aeducar, Aramoodle, fpadistancia, wordpress, Aularagón, etc... Dentro de CATEDU recientemente se ha constituido el equipo de competencia digital docente que persigue la formación y acreditación en competencia digital del profesorado, siguiendo esos mismos criterios.

-   Web: <https://catedu.es/>

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso
