---
layout: 2023/post
section: proposals
category: talks
author: Agustín Benito Bethencourt
title: Cobertura legal para mi proyecto de software libre&#58; algunas consideraciones en la toma de esta decisión
---

# Cobertura legal para mi proyecto de software libre: algunas consideraciones en la toma de esta decisión

>Muchos proyectos de software libre en crecimiento, o incluso algunos desde su inicio, sienten la necesidad de proporcionar cobertura legal al proyecto y sus contribuidores. Existen diferentes variables que deben ser consideradas a la hora de elegir entre las diferentes opciones existentes para dar esa cobertura. Esta charla describirá algunas de esas variables y la respuesta que se le da a las mismas desde La Eclipse Foundation, incluyendo un ejemplo real, el proyecto Oniro.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Idioma: Español

-   Descripción:

>Cuando se inicia un proyecto de software libre, especialmente si se tiene una clara orientación comercial, o cuando el proyecto alcanza cierta madurez, sus líderes, así como las posibles entidades que lo respaldan, se enfrentan a la necesidad de darle cobertura legal a ese proyecto, sus contribuidores y usuarios.<br><br>
Agustín, tras realizar una breve descripción de ese momento,con el fin de poner a la audiencia en situación, va a describir algunas de esas variables menos obvias que resultan ser relevantes en momentos clave del ciclo de vida de cualquier organización que da soporte a un proyecto de software libre. El ponente describirá cómo fundaciones mutiproyecto, como la Eclipse Foundation, aportan soluciones probadas a esas variables, así como a muchas otras relacionadas con ellas.<br><br>
Finalmente, Agustín invertirá unos minutos en dar a la audiencia unas pinceladas sobre lo que es la Eclipse Foundation, el proyecto en el que está actualmente involucrado, llamado Oniro (<http://oniroproject.org>) y cómo la Eclipse Foundation le da soporte.

-   Web del proyecto: <http://www.eclipse.org>

-   Público objetivo:

>Desarrolladores que quieren comenzar un proyecto de software libre o son líderes en proyectos que están considerando el establecimiento de una entidad legal para darle cobertura, responsables de empresas u otras organizaciones que dan soporte a proyectos de software libre y amantes del software libre en general.

## Ponente:

-   Nombre: Agustín Benito Bethencourt

-   Bio:

>Agustín (toscalix) es un profesional del Software Libre que ha trabajado como gestor o consultor para diversas organizaciones vinculadas a diferentes sectores, fundamentalmente en el área de sistemas operativos y plataformas. Algunas de esas organizaciones son ASOLIF, SUSE, Linaro o MBition (Mercedes-Benz). Asimismo ha estado involucrado a organizaciones/proyectos de software libre como GENIVI (hoy COVESA), CIP o AGL (ambos de La Linux Foundation) o KDE.<br><br>
Agustín trabaja en remoto desde Málaga y San Miguel de La Palma, Canarias y en la actualidad es Oniro Program Manager en La Eclipse Foundation. Para más información, visite <http://www.toscalix.com>.

### Info personal:

-   Web personal: <http://www.toscalix.com>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@toscalix>
-   Twitter: <https://twitter.com/@toscalix>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/toscalix>

## Comentarios

>Uno de los puntos más interesantes que describiré es en modelo de governanza que establece la Eclipse Foundation para proyectos (comunidades) y empresas (ecosistemas). Es muy interesante a la par que desconocido.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
