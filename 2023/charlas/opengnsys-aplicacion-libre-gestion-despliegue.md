---
layout: 2023/post
section: proposals
category: talks
author: Juan Carlos García López
title: OpenGnsys&#58; Aplicación libre y abierta para la gestión y el despliegue de sistemas operativos
---

# OpenGnsys: Aplicación libre y abierta para la gestión y el despliegue de sistemas operativos

>El Proyecto OpenGnsys (léase OpenGénesis) reúne el esfuerzo conjunto de varias Universidades Públicas Españolas, para disponer de una serie de herramientas libres y abiertas que constituyan un sistema completo, versátil e intuitivo, para la gestión y clonación de equipos. Este sistema permite la distribución, instalación y despliegue de distintos sistemas operativos.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>OpenGnsys es utilizado en un elevado número de universidades españolas para la gestión centralizada de aulas de informática, permitiendo la automatización de la instalación y configuración de software en las mismas mediante el despliegue de imágenes de sistema operativo desde una consola web de administración.

-   Web del proyecto: <https://www.opengnsys.es>

-   Público objetivo:

>Profesionales del sector informático que se dediquen a gestionar salas de informática: universidades, institutos, colegios, academias, asociaciones, ...

## Ponente:

-   Nombre: Juan Carlos García López

-   Bio:

> Trabajador del servicio de informática y comunicaciones de la Universidad de Zaragoza. Miembro del grupo de desarrollo del proyecto OpenGnsys.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
