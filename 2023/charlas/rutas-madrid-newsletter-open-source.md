---
layout: 2023/post
section: proposals
category: talks
author: Jorge Aguilera
title: Rutas por Madrid, una news-letter open source
---

# Rutas por Madrid, una news-letter open source

>Usando el catálogo de datos abiertos del ayuntamiento de Madrid quiero montar una news-letter con propuestas de rutas para visitar edificios singulares

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:
> El ayuntamiento de Madrid pública un catálogo de datos con información sobre monumentos y edificios de interés, así mismo tienen un site con esta información pero las fichas se visualizan de forma individual.<br><br>
Usando estos datos me he propuesto crear una news-letter semanal donde cada domingo se elija uno de ellos de forma aleatoria y se proponga una ruta para visitar los 10 sitios más cercanos. La news-letter enviará una ficha con los datos y se publicará una entrada por cada una con un mapa que muestre la ruta propuesta

-   Web del proyecto: <https://rutas-por-madrid.es>

-   Público objetivo:

>Es una charla sin complejidades técnicas que puede servir como inspiración para otras ideas.

## Ponente:

-   Nombre: Jorge Aguilera

-   Bio:

>Programador backend que disfruta contando las cosas que se le ocurren.

### Info personal:

-   Web personal: <https://jorge.aguilera.soy>
-   Mastodon (u otras redes sociales libres): <https://social.aguilera.soy/jorge>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/jorge-aguilera>

## Comentarios

El proyecto está empezado y espero tenerlo en marcha pronto.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
