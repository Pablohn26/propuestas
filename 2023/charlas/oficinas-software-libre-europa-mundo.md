---
layout: 2023/post
section: proposals
category: talks
author: Ana Jiménez Santamaría
title: Oficinas de Software Libre en Europa y el mundo&#58; Pasado, Presente y Retos de Futuro
---

# [PANEL] Oficinas de Software Libre en Europa y el mundo: Pasado, Presente y Retos de Futuro

>En un entorno altamente cambiante como es el tecnológico, no es de extrañar encontrar Oficinas de Software Libre / OSPOs más longevas y/o competentes que otras cuando observamos la sostenibilidad a largo plazo de estas oficinas. Esta realidad nos hace preguntarnos cosas como:
* ¿Existen factores que puedan diferenciar OSPO competitiva frente a otra destinada al olvido?
* ¿Cómo ha sido la evolución de OSPOs de Norte América, Europa, Asia u Oceanía?
* ¿Cómo ha sido la evolución de OSPOs en el sector académico, público y empresarial?
* ¿Qué podemos aprender de cada una de ellas?
* ¿Cómo podemos implementar enseñanzas de otras OSPOs a la hora de crear una en una organización?<br><br>
La audiencia podrá aprender más sobre qué es una OSPO en diferentes contextos, así como ingredientes comunes para crear una y los retos que tendrán que resolver en el futuro para asegurar la sostenibilidad de esta. Al final del panel, daremos paso a una ronda de preguntas y foro de debate en abierto.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Remoto
-   Idioma: Español

-   Descripción:

>Una Oficina de Software Libre actúa como el “centro de las operaciones de software libre en una organización”. A pesar de su reciente popularidad, las también llamadas OSPOs (Open Source Program Offices) ya tienen casi dos décadas de antigüedad.<br><br>
Sin embargo, su evolución, estructura y objetivos varían considerablemente ya que estas pueden surgir de acuerdo al tipo de organización (Empresa, Universidad, Gobierno, ONG, etc), marco cultural y económico (región, procesos internos de la organización), y un largo etc. Por ejemplo, mientras la principal misión de una Oficina de Software libre en un gobierno podría ser servir a la ciudadanía del país, la misión de una OSPO en una empresa puede enfocarse más a la retención de talento o sostenibilidad de proyectos/tecnologías clave para su negocio, así como la de una universidad puede girar en torno a la incorporación en procesos educativos e investigadores de herramientas y procesos de software libre.<br><br>
En un entorno altamente cambiante como es el tecnológico, no es de extrañar encontrar OSPOs más longevas y/o competentes que otras cuando observamos la sostenibilidad a largo plazo de estas oficinas. Esta realidad nos hace preguntarnos cosas como:
* ¿Existen factores que puedan diferenciar OSPO competitiva frente a otra destinada al olvido?
* ¿Cómo ha sido la evolución de OSPOs de Norte América, Europa, Asia u Oceanía?
* ¿Cómo ha sido la evolución de OSPOs en el sector académico, público y empresarial?
* ¿Qué podemos aprender de cada una de ellas?
* ¿Cómo podemos implementar enseñanzas de otras OSPOs a la hora de crear una en una organización?<br><br>
En este panel, un grupo de personas con experiencia en Oficinas de Software Libre responderán a estas y más preguntas, dando su visión sobre cómo las organizaciones han implementado y nutrido estas oficinas a los largo de los años. Prestando especial interés en:<br><br>
* La OfiLibres en España: Oficinas de Software Libre académicas y gubernamentales
* OSPOs Empresariales (Europa, Asia, América)<br><br>
La audiencia podrá aprender más sobre qué es una OSPO en diferentes contextos, así como ingredientes comunes para crear una y los retos que tendrán que resolver en el futuro para asegurar la sostenibilidad de esta. Al final del panel, daremos paso a una ronda de preguntas y foro de debate en abierto. Animamos a cualquier persona interesada y/o experiencia en oficinas de software libre a compartir su historia durante este espacio.<br><br>
Esta charla es un panel con los siguientes panelistas:<br><br>
* Jesús Barahona
* Ana Jiménez
* Manuel Palomo
* Josep Prat
* José Manrique López

-   Público objetivo:

>La audiencia podrá aprender más sobre qué es una OSPO en diferentes contextos, así como ingredientes comunes para crear una y los retos que tendrán que resolver en el futuro para asegurar la sostenibilidad de esta.

## Ponente:

-   Nombre: Ana Jiménez Santamaría

-   Bio:

>Ana trabaja como directora de proyecto en el TODO Group, un proyecto alojado en la Linux Foundation cuya comunidad de practicantes desean colaborar en buenas prácticas y herramientas para la formación y sostenibilidad de oficinas de open source que fomenten una colaboración sana de código abierto en organizaciones privadas y públicas. Anteriormente, trabajó en Bitergia, una empresa de análisis de desarrollo de software. En este tiempo, terminó su Máster en ciencia de datos, cuya tesis final se centró en medir el éxito de DevRel dentro de las comunidades de desarrollo de código abierto.<br><br>
Ana tiene un gran interés en Open Source, InnerSource y métricas de comunidad de desarrolladoras/es en entornos de software libre. Ha sido ponente en algunas conferencias internacionales como GitHub Universe, FOSSBackstage Berlin, Open Compliance Summit Japan, DevRelCon Tokyo, OpenInfraDays, DevRelCon London, ISC Summit y OSSummit (Japón, Europa y NA).<br><br>
En su tiempo libre, puedes entoncrar a Ana practicando yoga, avanzando con el Japonés o preparando videos tutoriales de ilustración con Krita e Inkscape.

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://fosstodon.org/@anajsana>
-   Twitter: <https://twitter.com/@anajsana95>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/anajimenezsantamaria> / <https://github.com/anajsana>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
