---
layout: 2023/post
section: proposals
category: talks
author: Jorge Aguilera
title: Geo posicionando canciones de Madriz
---

# Geo posicionando canciones de Madriz

>Proyecto colaborativo para geolocalizar canciones que hablen de esta ciudad. Cualquiera puede añadir su canción en el repositorio y se posicionará automáticamente

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>Un buen día alguien de Mastodon se le ocurrió compartir su lista de canciones relacionadas con Madrid y a otro usuario que podríamos hacer un mapa geolocalizandolas.<br><br>
De ahí nacio la idea de crear un repo git donde cualquiera puede añadir una canción que hable de Madrid proporcionando unas coordenadas y la aplicación crea un mapa asignándole un "área de influencia".

-   Web del proyecto: <https://github.com/jagedn/madriz-geocanciones> / <https://github.com/jagedn/tocamelamadriz>

-   Público objetivo:

>Cualquiera. Simplemente quiero mostrar cómo podemos colaborar sin muchos conocimientos técnicos en proyectos.

## Ponente:

-   Nombre: Jorge Aguilera

-   Bio:

>Software developer apasionado del Open Source.

### Info personal:

-   Web personal: <https://jorge.aguilera.soy>
-   Mastodon (u otras redes sociales libres): <https://social.aguilera.soy/jorge>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/jorge-aguilera>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
