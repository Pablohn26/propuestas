---
layout: 2023/post
section: proposals
category: talks
author: Carmen Delgado
title: Primeros pasos como gestora de una comunidad open-source
---

# Primeros pasos como gestora de una comunidad open-source

>Qué hacer y que no hacer cuando estamos comenzando como gestores  / promotores de comunidades open-source, cómo gestionar los diferentes personas de interés dentro de proyecto para conseguir los objetivos acordados.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Tengo experiencia ayudando a gestionar comunidades con Step4ward / BCN ENG, pero ahora desde Octubre 2022 estoy con Adoptium una comunidad con un colectivo muy colaborativo pero que muy bien constituida desde 2017, en el proceso he tenido que seguir avanzando en mis conocimientos en cuanto a gestión de personas de interes del proyecto para poder tomar decisiones, cómo plantear las oportunidades, como hacer seguimiento, cómo hacer entender tus necesidades y las desde punto de vista de negocio para la gestión de un proyecto. Quiero compartir mi experiencia para otras personas que están comenzando y también aprender de otras.

-   Web del proyecto: <https://adoptium.net/>

-   Público objetivo:

>Miembros de comunidades y otros gestores de comunidades

## Ponente:

-   Nombre: Carmen Delgado

-   Bio:

>Desde octubre 2022 soy la community manager de Eclipse Adoptium desde la Eclipse Foundation, con experiencia en gestión de PYMEs, ONGs, y empresas tecnológicas en ámbitos que van desde salud, investigación clínica, fintech, desarrollo de software y ahora open-source. Desde el 2020 soy co-fundadora de una grupo de mujeres voluntarias que ayudamos a mujeres que están empezando en el sector tecnológico (Step4ward) creado dentro de la comunidad BCN Engineering, con Step4ward he participado en paneles y talleres en eventos en Barcelona y también hemos creado sesiones virtuales y desde que estoy en Adoptium también he participado en la creación de contenido de forma virtual para dar a conocer al comunidad.

### Info personal:

-   Web personal: <https://www.linkedin.com/in/carmenldelgadop/>
-   Twitter: <https://twitter.com/cldelgadop>

## Comentarios

Me gustaría generar debate tanto de las personas dentro de la comunidad como aquellos que tenemos que gestionar.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
