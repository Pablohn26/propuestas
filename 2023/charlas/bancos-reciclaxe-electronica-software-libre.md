---
layout: 2023/post
section: proposals
category: talks
author: Martina Páramos Rial y José Antonio González Nóvoa
title: Bancos de Reciclaxe Electrónica con Software Libre
---

# Bancos de Reciclaxe Electrónica con Software Libre

>Los Bancos de Reciclaxe Electrónica con Software Libre tienen varios objetivos:
- Difusión de las tecnologías libres y la soberanía digital
- Promoción de la electrónica ética, mediante la reutilización de equipos electrónicos con el fin de aumentar su vida útil. Esto implica una disminución tanto de los residuos generados como de la extracción de materiales requeridos en la fabricación
- La reducción de la brecha digital
<br><br>Actualmente existen 5 bancos en Galicia, que surgen de la colaboración de varias asociaciones y de la implicación de ONG de carácter social, que son las que nos permiten canalizar los equipos hacia las personas que los necesitan. Impartimos formaciones a las personas usuarias y talleres de reparación. En los últimos meses dimos charlas en varios institutos, llamando la atención sobre la procedencia de los materiales empleados en la fabricación, y lo que ocurre con su eliminación. Todos los bancos están gestionados por personas voluntarias (unas 25 en toda Galicia).

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Lo que buscamos con la charla es la difusión del proyecto, buscando que más personas/asociaciones se animen a montar su propio banco allá donde estén. También se busca difundir los beneficios del software libre y la importancia de tener en cuenta componentes más sociales y éticas en el ámbito tecnológico. Este año estuvimos en el Congreso Estatal de Voluntariado presentando el proyecto, así como en la pasada edición del esLibre organizada en Vigo (donde tenemos unos de los bancos).

-   Web del proyecto: <https://galicia.isf.es/bancos-de-reciclaxe-electronica-con-software-libre/>

-   Público objetivo: Todo tipo de personas, especialmente aquellas personas/asociaciones con más interés en la parte social y ética de la tecnología.

## Ponente:

-   Nombre: Martina Páramos Rial y José Antonio González Nóvoa

-   Bio:

>Somos dos estudiantes de ingeniería con interés en la parte más social y ética de esta. Somos voluntarixs de la asociación Enxeñería Sen Fronteiras, participando en diversas actividades relacionadas con la aplicación de la tecnología para el bien común.

### Info personal:

-   Web personal: <https://galicia.isf.es/>
-   Twitter: <https://twitter.com/@ESFGALICIA>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
